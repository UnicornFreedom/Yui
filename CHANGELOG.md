## 0.23.0
* Improved `~u` characters support with extended Unicode place and whitespace characters  
(which you can surround with quotes in order not to be trimmed)
* Added `~namegen` alias to `~random:name`

## 0.22.2
* Apparently, website titles can have line breaks... Fixed.

## 0.22.1
* Added cookie banner bypassing to the `~title` action (to correctly show YouTube titles)

## 0.22.0
* Added `~romaji` action to convert kanji and kana (katakana, hiragana) to romaji

## 0.21.1
* Google search fix: bypassing cookies banners

## 0.21.0
* Added `~crab` action
* Fixed spelling

## 0.20.2
* Added `~shrug` emoji

## 0.20.1
* Fixed nickname validation for `~hook`

## 0.20.0
* Added `~random` action for generation of random numbers and names (`~random:name`)
* Warning when user forgets the target nickname for `~hook`
* `~tell` alias for `~hook` (since **brote** is no longer with us)

## 0.19.4
* Added `~ban:silent` modifier

## 0.19.3
* Fixed the `~hook` command to trigger hooks both with messages and commands
* Fixed the commands in `~help` repeated twice

## 0.19.2
* Store the `~hook`s in the database (not in the RAM anymore, persistence™)
* Upgrage the `~changelog` action a little (can show changes by version number now)

## 0.19.1
* Fixed `~hook` command aliases

## 0.19.0
* `~help` is here, finally
* `~lichess` now allows filtering results by game type (`~lichess:blitz` for example)
* `~title` command now works with arrows properly
* `~udef` output is now limited to 5 lines (no more flood). Also flood limit can be specified in the config
* `~mtg` will now print error message in case of timeout
* Overall optimization, upgraded dependencies

## 0.18.0
* `~q:delete` - admin-only option for quotes removal
* `~lichess` action to show user info from Lichess
* `~beep` alias for `~everybody`
* `~ban` and `~unban` to edit the user blacklist
* `~blog`, `~discord`, `~joined`, `~wednesday` and `~communism` actions
* Basic support for PRB currency (without history)
* Fixes and improvements

## 0.17.2
* `~mousu` action
* Fixed: unknown commands handler

## 0.17.1
* Fixed: `~everyone` must notify only users from the current channel

## 0.17.0
* `~morse` encoding
* `~nato` phonetic alphabet (army-like message spelling)
* `~everyone` action (mass highlight)
* New permisson setting - "admin commands"

## 0.16.2
* Fixed: Google search

## 0.16.1
* Fixed: `~weather` command causing SQL exception

## 0.16.0
* New `~top` command to get info about the most popular Yui commands
* `~weather` command will remember your location
* `~hook` now have `~send` and `~give` aliases
* Code cleaning, fixes and optimizations

## 0.15.0
* Added `~urban` command to fetch Urban Dictionary definitions
* Fixed (hopefully) the `~search` command
* Minor fixes & updates

## 0.14.2
* Updated `~mtg` action to show URL of the card description in the Wizards database
* Fixed `~mtg` throwing exception when searching for a card without mana cost
* Fixed `~plasma` and `~rainbow` commands for Unicode characters outside the Basic Multilingual Plane
* Fixed `~base64` command, which encoded only the first word of the given phrase
* Fixed `~lua` command: now all output must be trimmed to fit IRC maximal message size

## 0.14.1
* Replaced {} with () for mana cost in `~mtg` command

## 0.14.0
* Added `~mtg` action
* Fixed `~cite` formatting
* Updated `~o` command, added support for russian letters, `squared` and `combined` flavours

## 0.13.0
* Links to GitHub and GitLab
* Rainbows
* Alias for the BitCoal cryptocurrency
* `~away` alias to `~bye` command
* Blacklisted users will not be processed by `~title` background processing
* When life gives you `~lemons`, don't make lemonade
* The `~say` action now works with whole phrases, not just single words

## 0.12.0
* `~plot` command to generate random story plots
* `~hel`, `~hpm` and `~install hpm` commands
* New `~usd`, `~eur`, `~czk` aliases for `~coin` command
* Allow getting XKCD comics by ID or fetch the last one
* Fixed messages trimming (UTF-8 support)
* Refactored default palette (removed gray for errors)
* Minor overall improvements and fixes

## 0.11.0
* `~hash` function with support for MD5, SHA-1 and SHA-256 algorithms
* `~mastodon` command (#cc.ru channel on mastodon.social)
* `~changelog`command (link to this file on GitLab)
* `~ip`, `~lookup` and `~whois` aliases for the `~geo` action
* Added support for russian words for the `~def` command
* Upgraded command blacklisting (now you do not need to blacklist all aliases, one will be okay)
* Renamed config file to `yui.properties`
* Refactored commands syntax: added support for a `command:modifier`
* Fixed repeated search results from the `~law` command
* Fixes `~hook` action bugs (missing hooks)
* Fixed HTTPS URL's title resolving (thanks Fingercomp!)

## 0.10.0
* `~ocelot` command (for now just with a reference)
* `~uk` command to search by russian criminal code articles
* `~base64 decode` and `~base64 encode` commands
* `~zalgo` command to make the world around more chthonic
* `~vc` command for local GTA fans
* `~duck` command which gives access to DuckDuckGo instant answers API
* Added the possibility to cancel `~mq` with `~cancel` command

## 0.9.1
* Fixed `~wiki` output in the cases when the article snippet contains
multiple whitespaces in the middle.

## 0.9.0
* Added support for russian language to the `~rhyme` command
* Added `~anagram` command
* Fixed bugs in `~cite` action
* Added `~tell` and `~gitlab` aliases
* Added info about domain provider to the `~geo` output
* Added `~time` by hostname

## 0.8.0
* Added an alias `~locate` for the `~geo` command
* Support for the cases when user accidentally puts whitespace before the command
* Upgraded `~?` to answer on all unrecognized commands that ends with a `?`
* Removed one-line quotes addition, and replaced it with fetching quotes by ID
* Source code refactoring

## 0.7.0
* Added `~geo` command for getting geolocation by hostname

## 0.6.1
* Fixed `~anime` and `~hook` commands

## 0.6.0
* Added arrow modifier to the `~title` command (now you can do things like `~title ^`)
* Upgraded `~coin` command to understand constructions like `~btc to rub`
* Added `~turbofish`
* Changed versioning scheme to be more in the spirit of semver
* Refactoring & bugfixes (geolocation, command names conflicts)

## 0.5.4
* Added redirection to corresponding brote commands in case of misdirection
* Added geolocation by user IP to the `~weather` command 
* Allowed to `~tt` and `~t9` custom given phrases
* Excluded user nicknames from `~tt` command conversion
* Refactoring (annotation-based commands registration) & fixes (commands case-sensitivity)

## 0.5.3
* Added `~nocode`, `~neko` and `~swag` commands
* Updated `~hook` command to be more concise
* Added integration to GitLab CI
* Updated dependecies

## 0.5.2
* A lot of stuff
