package totoro.yui.actions

import totoro.yui.Config
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict

@Suppress("unused")
class BlacklistAction(private val configPath: String) : SensitivityAction("ban", "unban") {
    private val pardoned = Dict.Success + "pardoned" + "amnesty granted"
    private val banned = Dict.Success + "banned" + "blacklist updated"

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val target = command.args.firstOrNull()
        if (target != null) {
            val unban = command.name == "unban"
            val silent = command.name == "ban" && command.modifier == "silent"
            val ban = !unban && !silent
            val isBanned = Config.blackUsers.contains(target)
            val isSilent = Config.blackUsersSilent.contains(target)

            if (ban) {
                when {
                    isBanned -> client.send(command.chan, "already banned")
                    isSilent -> {
                        Config.blackUsersSilent.remove(target)
                        Config.blackUsers.add(target)
                        Config.save(configPath)
                        client.send(command.chan, "changed silent ban to a normal one")
                    }
                    else -> {
                        Config.blackUsers.add(target)
                        Config.save(configPath)
                        client.send(command.chan, banned())
                    }
                }
            }
            if (silent) {
                when {
                    isBanned -> {
                        Config.blackUsers.remove(target)
                        Config.blackUsersSilent.add(target)
                        Config.save(configPath)
                        client.send(command.chan, "moved to the silent ban section")
                    }
                    isSilent -> client.send(command.chan, "already banned (silent mode)")
                    else -> {
                        Config.blackUsersSilent.add(target)
                        Config.save(configPath)
                        client.send(command.chan, banned())
                    }
                }
            }
            if (unban) {
                when {
                    isBanned -> {
                        Config.blackUsers.remove(target)
                        Config.save(configPath)
                        client.send(command.chan, pardoned())
                    }
                    isSilent -> {
                        Config.blackUsersSilent.remove(target)
                        Config.save(configPath)
                        client.send(command.chan, pardoned())
                    }
                    else -> client.send(command.chan, "not yet banned")
                }
            }
        } else {
            client.send(command.chan, "who is our target?")
        }
        return true
    }

    override val description = "Yui's blacklist. For bad people who cannot be trusted with any commands. Admin only."
}
