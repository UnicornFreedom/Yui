package totoro.yui.actions

import totoro.yui.Yui
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import kotlin.math.max
import kotlin.math.min

@Suppress("unused")
@Action
class TotoroAction : SensitivityAction("totoro") {

    private val maxSquadSize = 15
    private val totoro = arrayOf(
            arrayOf(" _//|", "/oo |", "\\mm_|"),
            arrayOf("|\\\\_ ", "| oo\\", "|_mm/")
    )

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val quantity = max(1, min(maxSquadSize,
            if (command.args.isEmpty()) 1
            else command.args.first().toIntOrNull() ?: 1
        ))
        client.send(command.chan, generate(quantity))
        return true
    }

    private fun generate(quantity: Int): List<String> {
        val squad = arrayOf(StringBuilder(), StringBuilder(), StringBuilder())
        for (i in 1..quantity) {
            totoro[Yui.Random.nextInt(totoro.size)].zip(squad).forEach { (part, builder) -> builder.append("$part ") }
        }
        return squad.map { it.toString() }
    }

    override val description = "The Totoro."
}
