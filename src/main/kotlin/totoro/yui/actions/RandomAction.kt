package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.NameGen
import java.lang.NumberFormatException
import kotlin.math.min
import kotlin.math.roundToLong

@Suppress("unused")
@Action
class RandomAction : SensitivityAction("random", "r", "rand", "rn", "rnd", "rng", "name", "word", "namegen") {

    private val maxSyllables = 50

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        try {
            if (command.name == "word" || command.name == "name" || command.name == "namegen" ||
                command.modifier == "name" || command.modifier == "namegen" || command.modifier == "word") {
                val len = if (command.args.isEmpty()) null else command.args.first().toInt()
                if (len != null) client.send(command.chan, NameGen.name(min(len, maxSyllables)))
                else client.send(command.chan, NameGen.name())
            } else {
                val from = if (command.args.size > 1) command.args.first().toDouble() else 0.0
                val to =
                    (if (command.args.size > 1) command.args[1] else if (command.args.isNotEmpty()) command.args.first() else null)?.toDouble()
                val decimal =
                    (command.args.isNotEmpty() && command.args[0].contains('.')) ||
                            (command.args.size > 1 && command.args[1].contains('.')) ||
                            command.args.isEmpty()

                var result = Math.random()
                if (to != null) result = result * (to - from) + from

                if (!decimal) client.send(command.chan, result.roundToLong().toString())
                else client.send(command.chan, result.toString())
            }
        } catch (e: NumberFormatException) {
            client.send(
                command.chan,
                "${F.Red}the arguments are supposed to be numbers (or to be omitted)${F.Reset}"
            )
        }
        return true
    }

    override val description =
        "Generates random values. ~random [x] [y] will give you a random number. Also ~random:name [len] " +
                "(~random:word [len]) is available."
}
