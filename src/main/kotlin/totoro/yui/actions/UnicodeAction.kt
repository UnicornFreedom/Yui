package totoro.yui.actions

import org.jetbrains.kotlin.daemon.common.trimQuotes
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.UnicodeTable

@Suppress("unused")
@Action
class UnicodeAction : SensitivityAction("u", "char", "unicode") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.args.isEmpty()) {
            client.send(command.chan, "gimme some clues")
        } else {
            val itIsCode = command.content.length > 1 && (
                command.content[1] == '+' || command.content[1] == 'x' || command.content[1] == 'X')
            if (itIsCode) {
                try {
                    val code = Integer.parseInt(command.content.drop(2), 16)
                    client.send(command.chan, "${command.content}: ${F.Yellow}${Character.toString(code)}${F.Reset}")
                } catch (e: Exception) {
                    client.send(command.chan, "i've got an error and i'm confused")
                }
            } else {
                val contentIsInQuotes =
                    (command.content.startsWith("\"") || command.content.startsWith("'")) &&
                    (command.content.endsWith("\"") || command.content.endsWith("'"))
                val content = if (contentIsInQuotes) command.content.trimQuotes() else command.content
                UnicodeTable.get(content, { list ->
                    client.send(
                            command.chan,
                            list.filter { symbol -> symbol.symbol.isNotEmpty() && symbol.description.isNotEmpty() }
                                    .joinToString(", ") { symbol ->
                                        "${symbol.description} " +
                                                "(" + F.Yellow + "${symbol.code}: ${symbol.symbol}" + F.Reset +
                                                (if (symbol.symbol.length > 1) "  " else " ") + ")"
                                    }
                    )
                }, {
                    client.send(command.chan, "no characters found")
                })
            }
        }
        return true
    }

    override val description = "Search by unicode letter, letter code (U+...) or letter description."
}
