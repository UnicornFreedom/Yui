package totoro.yui.actions.data

/**
 * Action usage counter for stats
 */

class Popularity(val name: String, val value: Long)
