package totoro.yui.actions

import org.jetbrains.kotlin.backend.common.pop
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.api.Plot

@Suppress("unused")
@Action
class PlotAction : SensitivityAction("plot", "plots", "story") {

    private val plots = mutableListOf<String>()

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (plots.isEmpty()) plots.addAll(Plot.plots())
        if (plots.isNotEmpty()) client.send(command.chan, plots.pop())
        else client.send(command.chan, "no stories found")
        return true
    }

    override val description = "Generates random story lines for your hollywood blockbuster."
}
