package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.api.Changelog

@Suppress("unused")
@Action
class ChangelogAction : SensitivityAction("changelog", "changes") {
    private val pattern: Regex = "\\d+\\.\\d+\\.\\d+".toRegex()

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.args.isEmpty()) {
            client.send(command.chan, Changelog.getUrl())
        } else {
            val version = command.args.first()
            if (version.matches(pattern)) {
                Changelog.getChanges(version, { changes ->
                    client.send(command.chan, changes.content)
                }, {
                    client.send(command.chan, "For some reason I cannot download the changelog.")
                })
            } else {
                client.send(command.chan, "There is something unspeakable wrong with the version number you gave me...")
            }
        }
        return true
    }

    override val description = "Shows what's new. Use like this: ~changelog, or like this: ~changelog x.y.z where x.y.z " +
            "is the version you are interested in."
}
