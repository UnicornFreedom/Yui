package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict

@Suppress("unused")
@Action
class LuckyAction : SensitivityAction("islucky", "lucky") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val text = if (command.content.isBlank()) lucky(command.content) else "gimme something to estimate"
        client.send(command.chan, text)
        return true
    }

    private fun sum(value: String) = value.fold(0) { acc, ch -> acc + ch.toInt() }

    private fun lucky(value: String): String {
        val half = value.length / 2
        val success = sum(value.dropLast(half)) == sum(value.drop(half))
        return if (success) Dict.Yeah() else Dict.Nope()
    }

    override val description = "A word is considered `lucky` if the sum of first half character codes is equal to the " +
            "sum of the second half. Example: ~islucky me"
}
