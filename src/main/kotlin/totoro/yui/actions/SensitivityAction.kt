package totoro.yui.actions

import totoro.yui.actions.data.Priority
import totoro.yui.client.Command
import totoro.yui.client.IRCClient

/**
 * CommandAction extension that takes care of matching the command name
 */

abstract class SensitivityAction(protected val sensitivities: List<String>) : CommandAction {
    override var blocked = false
    override var adminsOnly = false

    /**
     * Default priority for all commands is INTERMEDIATE.
     * Override to change it.
     */
    override val priority = Priority.INTERMEDIATE

    /**
     * Sensitivity pattern can be a simple string (without whitespace characters),
     * or a regex pattern. In the last case the string must begin with `!` sign.
     */
    private val strings = sensitivities.filter { !it.startsWith("!") }
    private val regexes = sensitivities.filter { it.startsWith("!") }.map { it.drop(1).toRegex() }

    constructor(vararg sensitivities: String) : this(sensitivities.toList())

    override fun willProcess(client: IRCClient, command: Command): Boolean {
        return strings.contains(command.name) || regexes.any { it.matches(command.name.orEmpty()) }
    }

    /**
     * Compares first word of command with given list of sensitivities.
     * In case of any matches - runs `handle` function and 'absorb' the command.
     * Otherwise - returns the command, so the next action handler in queue can try it.
     */
    override fun processCommand(client: IRCClient, command: Command): Command? {
        val success = willProcess(client, command) && handleCommand(client, command)
        return if (success) null else command
    }

    /**
     * This function must be overridden in any child action class.
     * It is responsible for real actions in behalf of the command.
     * @return true if the command was handled successfully, and false otherwise
     */
    abstract fun handleCommand(client: IRCClient, command: Command): Boolean

    /**
     * For a [SensitivityAction] any of the action's sensitivities can be used as the action ID
     */
    override fun correspondsName(name: String?): Boolean {
        return sensitivities.contains(name)
    }

    override fun name(): String {
        return sensitivities.first()
    }

    protected open val description: String? = null

    override fun explanation(): String {
        return (if (description == null) "" else description + "\n") + "Aliases: $sensitivities"
    }
}
