package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.RandomIterator
import totoro.yui.util.api.Boulter

@Suppress("unused")
@Action
class AnagramAction : SensitivityAction("anagram", "anagrams") {

    private val maxAnagrams = 10
    private val maxInputLength = 50

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        return if (command.content.isNotBlank()) {
            if (command.content.length > maxInputLength)
                client.send(command.chan, "let's not do this")
            else {
                val anagrams = Boulter.anagrams(command.content)
                if (anagrams.isNotEmpty()) {
                    client.send(command.chan, F.Yellow + command.content + F.Reset + ": " +
                            RandomIterator(anagrams).take(maxAnagrams).joinToString(", "))
                } else client.send(command.chan, "no anagrams found")
            }
            true
        } else false
    }

    override val description = "Pretty self-explanatory. First argument is the source word (max. $maxInputLength letters). Example: ~anagram apple"
}
