package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.Datamuse

@Suppress("unused")
@Action
class DescribeAction : SensitivityAction("desc", "describe", "description") {

    private val maxAdjectives = 10

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        return if (command.args.isNotEmpty()) {
            val word = command.args.joinToString(" ")
            val adjectives = Datamuse.describe(word)
            if (adjectives.isNotEmpty())
                client.send(command.chan, F.Yellow + word + F.Reset + " can be: " +
                        adjectives.take(maxAdjectives).joinToString(", "))
            else client.send(command.chan, "no adjectives found")
            true
        } else false
    }

    override val description = "Gives you some words, that can be used to describe given thing. English only."
}
