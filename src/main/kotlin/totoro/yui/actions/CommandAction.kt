package totoro.yui.actions

import totoro.yui.actions.data.Priority
import totoro.yui.client.Command
import totoro.yui.client.IRCClient

/**
 * Action processors do almost all the actual work of this bot.
 * They process incoming commands and send corresponding messages in reply.
 * Each processor can consume given command, or transfer further, to the next processor.
 * CommandAction is a processor that works with prepared `command` instances.
 */

interface CommandAction: Explained {
    /**
     * Marks the command processor, that is blocked in the config.
     */
    var blocked: Boolean

    /**
     * Marks the command processor that is available only to admins.
     */
    var adminsOnly: Boolean

    /**
     * Determine the relative position of the processor in the command processing queue
     */
    val priority: Priority

    /**
     * Decide, whether given command corresponds to the action.
     */
    fun willProcess(client: IRCClient, command: Command): Boolean

    /**
     * If the given command corresponds to the action, it can `consume` the command, and return null.
     * Otherwise, it must return the same command back, so the next action processor in the queue can process it.
     * Note: The `processCommand` method is allowed not to process the commands, that give `true` from the
     * `willProcess`. In such cases the next processor in queue will take them.
     */
    fun processCommand(client: IRCClient, command: Command): Command?

    /**
     * Identify the command by name.
     * This may be slightly different from the [processCommand] method result, because we do not need to process
     * the command call arguments here, but we need to match the command object with some kind of string ID
     */
    fun correspondsName(name: String?): Boolean

    /**
     * Return the `official name` of the command. May be used to represent the command in all kind of TOP-X etc.
     * Usually it is the primary alias.
     */
    fun name(): String
}
