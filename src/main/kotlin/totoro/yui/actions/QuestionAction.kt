package totoro.yui.actions

import totoro.yui.actions.data.Priority
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict

@Suppress("unused")
@Action
class QuestionAction : CommandAction {
    override var blocked = false
    override var adminsOnly = false
    /**
     * This action must be processed close to the end, but not the last
     */
    override val priority = Priority.PENULTIMATE

    private val sensitivities = listOf("question", "?", "8", "8?", "ball")
    private val dict = Dict.Yeah + Dict.Nope + Dict.Maybe

    override fun willProcess(client: IRCClient, command: Command): Boolean {
        return (sensitivities.contains(command.name) || command.content.endsWith('?'))
    }

    override fun processCommand(client: IRCClient, command: Command): Command? {
        return if (willProcess(client, command)) {
            client.send(command.chan, dict())
            null
        } else command
    }

    override fun correspondsName(name: String?): Boolean {
        return sensitivities.contains(name)
    }

    override fun name(): String {
        return sensitivities.first()
    }

    override fun explanation(): String {
        return "Just ask your (yes-or-no) question."
    }
}
