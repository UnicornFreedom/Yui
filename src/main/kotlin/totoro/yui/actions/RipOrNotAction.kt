package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict
import java.util.*
import kotlin.math.abs

@Suppress("unused")
@Action
class RipOrNotAction : SensitivityAction("rip?", "rippo?", "ripped?") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val text = check(command.original + (Date().time / 3600000))
        client.send(command.chan, text)
        return true
    }

    private fun check(data: String): String = when (abs(data.hashCode() % 3)) {
        0 -> Dict.Yeah()
        1 -> Dict.Maybe()
        2 -> Dict.Nope()
        else -> Dict.Kawaii()
    }

    override val description = "This useful command helps you to determine if something is ripped or not today. " +
            "Example: ~ripped? my t-shirt"
}
