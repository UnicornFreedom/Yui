package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient

@Suppress("unused")
@Action
class MorseAction : SensitivityAction("morse") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        client.send(command.chan, encode(command.content))
        return true
    }

    private val rules = hashMapOf(
            '1' to ".----", '2' to "..---", '3' to "...--", '4' to "....-", '5' to ".....",
            '6' to "-....", '7' to "--...", '8' to "---..", '9' to "----.", '0' to "-----",
            '.' to "......", ',' to ".-.-.-", ':' to "---...", ';' to "-.-.-.", '(' to "-.--.-",
            ')' to "-.--.-", '\'' to ".----.", '"' to ".-..-.", '-' to "-....-", '/' to "-..-.",
            '?' to "..--..", '!' to "--..--", '@' to ".--.-.", '|' to "-...-",
            'A' to ".-", 'B' to "-...", 'C' to "-.-.", 'D' to "-..", 'E' to ".", 'F' to "..-.", 'G' to "--.",
            'H' to "....", 'I' to "..", 'J' to ".---", 'K' to "-.-", 'L' to ".-..", 'M' to "--", 'N' to "-.",
            'O' to "---", 'P' to ".--.", 'Q' to "--.-", 'R' to ".-.", 'S' to "...", 'T' to "-", 'U' to "..-",
            'V' to "...-", 'W' to ".--", 'X' to "-..-", 'Y' to "-.--", 'Z' to "--..", 'Ö' to "---.", 'Ñ' to "--.--",
            'É' to "..-..", 'Ü' to "..--", 'Ä' to ".-.-",
            'А' to ".-", 'Б' to "-...", 'В' to ".--", 'Г' to "--.", 'Д' to "-..", 'Е' to ".", 'Ё' to ".",
            'Ж' to "...-", 'З' to "--..", 'И' to "..", 'Й' to ".---", 'К' to "-.-", 'Л' to ".-..", 'М' to "--",
            'Н' to "-.", 'О' to "---", 'П' to ".--.", 'Р' to ".-.", 'С' to "...", 'Т' to "-", 'У' to "..-",
            'Ф' to "..-.", 'Х' to "....", 'Ц' to "-.-.", 'Ч' to "---.", 'Ш' to "----", 'Щ' to "--.-", 'Ъ' to "--.--",
            'Ы' to "-.--", 'Ь' to "-..-", 'Э' to "..-..", 'Ю' to "..--", 'Я' to ".-.-"
    )

    private fun encode(phrase: String): String {
        if (phrase.isEmpty()) return phrase
        return phrase.map {
            when {
                it in rules -> rules[it]!!
                it.toUpperCase() in rules -> rules[it.toUpperCase()]
                else -> it
            }
        }.joinToString(" ")
    }

    override val description = "Morse code encoder."
}
