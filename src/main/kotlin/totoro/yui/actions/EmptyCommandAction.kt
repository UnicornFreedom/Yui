package totoro.yui.actions

import totoro.yui.actions.data.Priority
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict

/**
 * This action triggers when the bot receives a command prompt with nothing else.
 */

@Suppress("unused")
@Action
class EmptyCommandAction : CommandAction {
    private val name = "empty_command"

    override var blocked = false
    override var adminsOnly = false
    override val priority = Priority.INTERMEDIATE

    val dict = Dict.Kawaii + Dict.NotSure

    override fun willProcess(client: IRCClient, command: Command): Boolean {
        return !command.valid
    }

    override fun processCommand(client: IRCClient, command: Command) = if (willProcess(client, command)) {
        client.send(command.chan, dict())
        null
    } else command

    override fun correspondsName(name: String?): Boolean {
        return name == this.name
    }

    override fun name(): String {
        return name
    }

    override fun explanation(): String {
        return "No documentation on this."
    }
}
