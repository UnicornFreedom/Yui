package totoro.yui.actions

import totoro.yui.Config
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict
import totoro.yui.util.F
import totoro.yui.util.StringHelper
import totoro.yui.util.api.Kitsu

@Suppress("unused")
@Action
class AnimeAction : SensitivityAction("a", "anime", "oneme", "animu", "onemu", "kitsu") {

    private val refusals = Dict.Refuse + "no anime today, losers"

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val anime = Kitsu.search(command.content)
        if (anime == null) client.send(command.chan, refusals())
        else {
            client.send(command.chan,
                    if (anime.nsfw) F.Red + "[NSFW] " else "" +
                    F.Yellow + "${anime.title} " + F.Reset +
                    "/ ${anime.episodeCount} episode${if (anime.episodeCount != 1) "s" else ""} ${anime.episodeLength}m " +
                    "/ ${anime.status} " +
                    "/ ${anime.rating} " +
                    "/ https://kitsu.io/anime/${anime.slug}"
            )
            val message = StringHelper.utf8trim(F.Italic + "(" + anime.synopsis,
                    Config.maxMessageLength - 8 - command.chan.length - client.getMessagePrefixLength(), true) + ")" + F.Reset
            client.send(command.chan, message)
        }
        return true
    }

    override val description = "Searches for anime on kitsu.io. Example: ~anime about totoro"
}
