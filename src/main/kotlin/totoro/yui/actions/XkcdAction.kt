package totoro.yui.actions

import totoro.yui.Config
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.StringHelper
import totoro.yui.util.api.Xkcd
import totoro.yui.util.api.data.Comics

@Suppress("unused")
@Action
class XkcdAction : SensitivityAction("x", "xkcd") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.args.isEmpty() || (command.args.size == 1 && command.args.first().all { it.isDigit() })) {
            val id = if (command.args.isEmpty()) null else command.args.first().toInt()
            Xkcd.getById(
                    id,
                    { comics -> printXkcd(comics, client, command.chan) },
                    { client.send(command.chan, "no xkcd for this") }
            )
        } else {
            Xkcd.search(
                    command.content,
                    { comics -> printXkcd(comics, client, command.chan) },
                    { client.send(command.chan, "no xkcd for this") }
            )
        }
        return true
    }

    private fun printXkcd(comics: Comics, client: IRCClient, channel: String) {
        client.send(channel,
                F.Yellow + comics.title + F.Reset +
                        " / https://xkcd.com/${comics.id}/"
        )
        val message = StringHelper.utf8trim(F.Italic + "(" + comics.alt,
                Config.maxMessageLength - 8 - channel.length - client.getMessagePrefixLength(), true) + ")" + F.Reset
        client.send(channel, message)
    }

    override val description = "XKCD search (by number or description)."
}
