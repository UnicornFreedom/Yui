package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F

@Suppress("unused")
@Action
class RainbowAction : SensitivityAction("rainbow", "rb", "pride") {

    private val colors = arrayOf(
            F.Red, F.Orange, F.Yellow, F.Green, F.Cyan, F.Blue, F.Purple, F.Blue, F.Cyan, F.Green, F.Yellow, F.Orange
    )
    private var index: Int = 0

    private fun nextColor(): String {
        val color = colors[index]
        index = (index + 1) % colors.size
        return color
    }

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val iter = command.content.codePoints().iterator()
        val payload = StringBuilder()

        payload.append(F.Bold)

        while (iter.hasNext()) {
            payload.append(F.mix(F.White, nextColor()))
            payload.append(Character.toChars(iter.nextInt()))
        }

        payload.append(F.Reset)

        client.send(command.chan, payload.toString())
        return true
    }

    override val description = "Do you feel the pride?"
}
