package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict
import totoro.yui.util.api.Pastebin

@Suppress("unused")
@Action
class HelpAction : SensitivityAction("help", "man", "h", "explain") {

    private val answers = Dict.Refuse + Dict.of(
            "please, somebody give this man some help!", "sorry, I cannot help",
            "a little help definitely would not hurt")

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.args.isEmpty()) {
            val builder = StringBuilder()
            client.getActionProcessorsIter().forEach { action ->
                builder.append("~")
                builder.append(action.name())
                builder.append("\n---\n")
                builder.append(action.explanation())
                builder.append("\n\n")
            }
            Pastebin.post(builder.toString(),
                    { key -> client.send(command.chan, "check this: https://hastebin.com/$key.md") },
                    { client.send(command.chan, answers()) })
        } else {
            client.send(command.chan,
                    client.getFirstActionProcessor(command.chan, command.user, command.content).explanation())
        }
        return true
    }

    override val description = "Gets help for those in need."
}
