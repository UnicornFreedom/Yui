package totoro.yui.actions

import io.magicthegathering.kotlinsdk.api.MtgCardApiClient
import io.magicthegathering.kotlinsdk.model.card.MtgCard
import totoro.yui.Log
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict
import totoro.yui.util.F
import java.lang.Exception

@Suppress("unused")
@Action
class MTGAction : SensitivityAction("mtg", "card", "cards", "magic") {
    private val refusals = Dict.Refuse + listOf("not enough mana", "i'm out of cards")
    private val pageSize = 1

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        try {
            val response = MtgCardApiClient.getCardsByPartialName(command.content, pageSize, 0)
            if (response.isSuccessful) {
                val cards = response.body()
                if (cards?.isNotEmpty() == true) {
                    cards.forEach { printCard(it, client, command.chan) }
                } else {
                    client.send(command.chan, refusals())
                }
            } else {
                client.send(command.chan, refusals())
            }
        } catch (e: Exception) {
            Log.error("Was not able to get an answer from MTG API: ")
            Log.debug(e)
            client.send(command.chan, "MTG API is unresponsive")
        }
        return true
    }

    private fun printCard(card: MtgCard, client: IRCClient, channel: String) {
        // manaCost field here has type String, but can be sometimes equal to null
        // do not ask me how it is possible
        client.send(channel, F.Yellow + card.name + F.Reset + " " +
            (card.manaCost.replace('{', '(').replace('}', ')')) + " " +
            card.type +
            (if (card.power != null && card.toughness != null) {
                " [" + card.power + "/" + card.toughness + "] "
            } else " ") + "/ " +
            card.rarity + " / " + "http://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=" + card.multiverseid
        )
        if (card.text != null) {
            client.send(channel, card.text ?: "")
        }
    }

    override val description = "Searches info about MTG cards. Example: ~mtg mindslaver"
}
