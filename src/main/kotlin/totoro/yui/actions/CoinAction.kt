package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.BankiPMR
import totoro.yui.util.api.CryptoCompare
import java.text.DecimalFormat
import java.time.Duration
import kotlin.math.abs

@Suppress("unused")
@Action
class CoinAction : SensitivityAction("coin", "cur", "currency",
        "btc", "bitcoin", "eth", "ether", "ethereum", "fcn", "fantom", "fantomcoin",
        "doge", "dogecoin", "neo", "neocoin", "monero", "xmr", "ripple", "coal",
        "usd", "eur", "czk", "prb", "rub") {

    private val longFormat = DecimalFormat("0.#####################")
    private val shortFormat = DecimalFormat("0.###")

    private fun isDelta(value: String): Boolean {
        return value[0] == 'p' && value[1].isDigit()
    }

    private fun getCurrencyCodes(args: List<String>): List<String> =
            args.filter { !it[0].isDigit() && !isDelta(it) }.map { it.toUpperCase() }

    private fun getDelta(args: List<String>): String? =
            args.firstOrNull { isDelta(it) }

    private fun getAmount(args: List<String>): Double? =
            args.mapNotNull { it.toDoubleOrNull() }.firstOrNull()

    private fun formatDelta(delta: Double): String {
        return "(" + when {
            delta > 0 -> F.Green + "▴" + F.Reset
            delta < 0 -> F.Red + "▾" + F.Reset
            else -> ""
        } + shortFormat.format(abs(delta)) + "%)"
    }

    override fun handleCommand(client: IRCClient, command: Command): Boolean {

        fun get(from: String, to: String, range: String?, amount: Double?) {
            val duration = if (range != null)
                try { Duration.parse(range.toUpperCase()) } catch (e: Exception) { null }
            else null

            if (range != null && duration == null) {
                client.send(command.chan, "try ISO 8601 for time duration")
            } else {
                val finalAmount = amount ?: 1.0
                val upperFrom = from.toUpperCase()
                val upperTo = to.toUpperCase()
                val prbConversion = upperFrom == "PRB" || upperTo == "PRB"
                val finalFrom = if (upperFrom == "PRB") "USD" else upperFrom
                val finalTo = if (upperTo == "PRB") "USD" else upperTo
                val usdToPrb = if (prbConversion) {
                    val coeff = BankiPMR.rate()
                    if (coeff == null) {
                        client.send(command.chan, "cannot get the rate for this")
                        return
                    }
                    coeff
                } else { 1.0 }
                CryptoCompare.get(
                        finalFrom,
                        finalTo,
                        duration,
                        { currency ->
                            var price = currency.price * finalAmount
                            if (prbConversion) {
                                if (upperFrom == "PRB") {
                                    price /= usdToPrb
                                } else {
                                    price *= usdToPrb
                                }
                            }
                            client.send(command.chan,
                                    "$finalAmount $upperFrom -> " +
                                    F.Yellow + longFormat.format(price) + F.Reset + " $upperTo" +
                                    if (currency.description != null) F.Italic + " (${currency.description})" + F.Reset else "" +
                                    if (range != null) "  " + formatDelta(currency.delta) else ""
                            )
                        },
                        {
                            client.send(command.chan, "cannot get the rate for this")
                        }
                )
            }
        }

        val codes = getCurrencyCodes(command.args)
        val delta = getDelta(command.args)
        val amount = getAmount(command.args)

        val from = when (command.name) {
            "btc", "bitcoin" -> "BTC"
            "eth", "ether", "ethereum" -> "ETH"
            "doge", "dogecoin" -> "DOGE"
            "neo", "neocoin" -> "NEO"
            "monero", "xmr" -> "XMR"
            "ripple" -> "XRP"
            "fcn", "fantom", "fantomcoin" -> "FCN"
            "coal" -> "COAL"
            "usd" -> "USD"
            "eur" -> "EUR"
            "czk" -> "CZK"
            "prb" -> "PRB"
            "rub" -> "RUB"
            else -> if (codes.isNotEmpty()) codes.first() else "USD"
        }

        val to = when {
            codes.isEmpty() || codes.last() == from -> if (from == "RUB" || from == "BTC") "USD" else "RUB"
            else -> codes.last()
        }

        get(from, to, delta, amount)

        return true
    }

    override val description = "Currency conversions. Works best for cryptocurrencies, but can be used for common ones " +
            "too. Use currency codes, like: ~coin BTC to RUB. You can get the price dynamics for given time period. " +
            "Use ISO 8601 for time intervals. Example: ~coin ETH p1d. Also you can specify quantity (~coin 10 DOGE) " +
            "and use short aliases for source currency: ~eur 2"
}
