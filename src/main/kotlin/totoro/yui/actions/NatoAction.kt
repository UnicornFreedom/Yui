package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient

@Suppress("unused")
@Action
class NatoAction : SensitivityAction("nato", "icao", "usa", "military", "army", "bravo") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        client.send(command.chan, encode(command.content))
        return true
    }

    private val rules = hashMapOf(
            'A' to "ALPHA", 'B' to "BRAVO", 'C' to "CHARLIE", 'D' to "DELTA", 'E' to "ECHO",
            'F' to "FOXTROT", 'G' to "GOLF", 'H' to "HOTEL", 'I' to "INDIA", 'J' to "JULIET",
            'K' to "KILO", 'L' to "LIMA", 'M' to "MIKE", 'N' to "NOVEMBER", 'O' to "OSCAR",
            'P' to "PAPA", 'Q' to "QUEBEC", 'R' to "ROMEO", 'S' to "SIERRA", 'T' to "TANGO",
            'U' to "UNIFORM", 'V' to "VICTOR", 'W' to "WHISKEY", 'X' to "X-RAY", 'Y' to "YANKEE", 'Z' to "ZULU",
            '1' to "ONE", '2' to "TWO", '3' to "THREE", '4' to "FOUR", '5' to "FIVE",
            '6' to "SIX", '7' to "SEVEN", '8' to "EIGHT", '9' to "NINE", '0' to "ZERO", '-' to "DASH", '.' to "STOP"
    )

    private fun encode(phrase: String): String {
        if (phrase.isEmpty()) return phrase
        return phrase.filter { !it.isWhitespace() }.map {
            when {
                it in rules -> rules[it]!!
                it.toUpperCase() in rules -> rules[it.toUpperCase()]
                else -> it
            }
        }.joinToString(" ")
    }

    override val description = "Message encoder in NATO-army style. ALPHA BRAVO CHARLIE!"
}
