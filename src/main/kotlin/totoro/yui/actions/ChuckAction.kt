package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.api.Chuck

@Suppress("unused")
@Action
class ChuckAction : SensitivityAction("chuck", "norris", "chucknorris") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        Chuck.quote({ client.send(command.chan, it) }) {
            client.send(command.chan, "chuck is sleeping")
        }
        return true
    }

    override val description = "Random Chuck Norris joke. Just for you."
}
