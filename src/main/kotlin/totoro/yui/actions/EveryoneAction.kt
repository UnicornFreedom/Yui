package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient

@Suppress("unused")
@Action
class EveryoneAction : SensitivityAction("everyone", "@", "everybody", "all", "beep") {
    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val nicknames = client.getListOfNicknamesOnline(command.chan).joinToString(" ")
        client.send(command.chan, nicknames)
        return true
    }

    override val description = "Gondor calls for aid!"
}
