package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.Wiki

@Suppress("unused")
@Action
class WikiAction : SensitivityAction("w", "wiki", "wikipedia") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        Wiki.search(command.content, command.modifier ?: "en",
                { article ->
                    client.send(command.chan, F.Yellow + article.title + F.Reset + " / " + article.url)
                    client.send(command.chan, F.Italic + article.snippet + F.Reset)
                },
                {
                    client.send(command.chan, "cannot find anything on the topic")
                }
        )

        return true
    }

    override val description = "Searches wikipedia.org for info. Use language modifier for language-specific search. Example: ~wiki:ru Яблоко"
}
