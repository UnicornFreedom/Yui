package totoro.yui.util

import kotlin.math.log10

object MathHelper {
    fun numberOfDigits(value: Long): Int {
        if (value == 0L) return 1
        if (value < 0) return numberOfDigits(-value) + 1
        return (log10(value.toDouble()) + 1).toInt()
    }
}
