package totoro.yui.util

import java.nio.charset.StandardCharsets
import kotlin.experimental.and

object StringHelper {
    private val multipleWhitespacesPattern = "\\s+".toRegex()

    fun deWhitespace(value: String?): String? {
        return value?.replace(multipleWhitespacesPattern, " ")
    }

    private const val tailMask: Byte = 0b11000000.toByte()
    private const val tailPattern: Byte = 0b10000000.toByte()

    fun utf8trim(value: String, maxLength: Int, dots: Boolean = false): String {
        val bytes = value.toByteArray(StandardCharsets.UTF_8)
        return if (bytes.size > maxLength) {
            var length = 0
            for (i in 0..bytes.size) {
                if (bytes[i] and tailMask != tailPattern)
                    if ((!dots && i < maxLength) || (dots && i < maxLength - 3)) length = i
                    else break
            }
            val result = bytes.sliceArray(0 until length).toString(StandardCharsets.UTF_8)
            return if (dots) "$result…"
            else result
        } else value
    }
}
