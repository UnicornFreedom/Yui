package totoro.yui.util

import totoro.yui.Yui

@Suppress("MemberVisibilityCanBePrivate")
class Dict<T>(val variants: List<T>) {
    companion object {
        val KawaiiEmoji = of(":3", "${F.Red}♥${F.Reset}", "(〜￣▽￣)〜", "uwu")
        val Kawaii = KawaiiEmoji + of("kawaii", "nya")
        val Hello = of("o/", "oi", "ahoy", "hey", "hi", "hello", "aloha", "안녕", "おはよう",
                "anyoung", "ohayou", "ohayou gozaimasu", "nyanpasu~", "nyanpasu", "howdy", "wazzup", "shalom")
        val Bye = of("bye", "goodbye", "farewell", "au revoir", "o/", "ciao", "sayonara", "shalom",
                "bon voyage", "auf Wiedersehen", "aloha", "have a good day", "take care", "bye bye", "later",
                "see you later", "peace")
        val Greets = Kawaii + Hello
        val Nope = of("nope", "no", "nay", "not", "no way", "of course not", "-", "i don't think so")
        val Yeah = of("yeah", "aye", "yes", "yep", "+", "definitely", "of course", "sure",
                "sure enough", "surely", "certainly", "naturally", "absolutely", "yea", "yup")
        val Maybe = of("maybe", "perhaps", "possibly", "it could be", "mayhap", "most likely", "there is a chance",
                "you don't want to know this", "you can say so", "depending on circumstances")
        val NotSure = of("?", "huh?", "i don't understand", "i don't know", "...", ":<", "ehhhhhh~",
                "gimme something to work with", "what is this?", "wtf?", "desu", "rippu desu",
                "INSUFFICIENT DATA FOR MEANINGFUL ANSWER", "nani?")
        val Excited = of("doki doki shiteru", "\\o/", "yay", "nice", "suki desu!", "daisuki desu!")
        val Upset = of(":<", "...", "v_v", "-_-")
        val Offended = of("baka") + Upset
        val Thanks = of("thanks", "thanks", "thank you", "thx", "thanks a lot", "cheers", "arigatou")
        val Refuse = of("ask totoro", "if only I can", "i will put that on my schedule",
                "gosh, I really wish I could", "I’m focusing on other things right now", "don't think so",
                "life is too short to do things you don't love", "i would love to, but unfortunately... no",
                "alas, such a task is no match for my incompetency", "i shall not", "i think not", "offer declined",
                "I’m trying to see how long I can go without saying yes", "in theory I would be interested, " +
                "in practice - no")
        val AcceptTask = of("okay", "consider it done", "no problem", "sure", "got it", "right away",
                "leave it to me")
        val Rip = of("rip", "rippo", "rust in peppers", "✝", "✞", "ＲＩＰ", "rip irc")
        val Restricted = of("sorry, this is too serious business", "you must be an admin to issue this command",
                "staff only", "unauthorized access prohibited", "well, you don't have a permission to do it",
                "access denied", "your access has been denied", "you shall not pass")
        val Success = of("done", "mission accomplished", "consider it done", "mission complete", "job is done")

        fun <T> of(vararg variants: T): Dict<T> = Dict(variants.asList())
    }

    operator fun plus(element: T) = Dict(variants + element)
    operator fun plus(new: List<T>) = Dict(variants + new)
    operator fun plus(other: Dict<T>) = Dict(variants + other.variants)

    operator fun invoke() = pick()
    operator fun invoke(n: Int) = pick(n)

    fun pick() = variants[Yui.Random.nextInt(variants.size)]
    fun pick(n: Int) = (1..n).map { pick() }
}
