package totoro.yui.util

enum class LogLevel {
    DEBUG, INFO, WARNING, ERROR;

    companion object {
        fun serialize(value: LogLevel): String {
            return when (value) {
                DEBUG -> "debug"
                INFO -> "info"
                ERROR -> "error"
                WARNING -> "warning"
            }
        }

        fun deserialize(raw: String): LogLevel {
            return when (raw) {
                "d", "debug" -> DEBUG
                "i", "info" -> INFO
                "w", "warning", "warn" -> WARNING
                else -> ERROR
            }
        }
    }
}
