package totoro.yui.util

import fr.free.nrw.jakaroma.Jakaroma

object Japanese {
    private val jakaroma = Jakaroma()

    fun romanize(text: String): String {
        return jakaroma.convert(text, false, false)
    }
}
