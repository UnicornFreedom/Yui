package totoro.yui.util.api

import org.jsoup.Jsoup

object Plot {
    private const val useragent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) " +
            "Chrome/59.0.3071.115 Safari/537.36"

    fun plots(): List<String> {
        return try {
            val doc = Jsoup.connect("https://words.bighugelabs.com/plot.php").userAgent(useragent).get()
            doc.select("li").map { it.text() }
        } catch (e: Exception) {
            listOf()
        }
    }
}
