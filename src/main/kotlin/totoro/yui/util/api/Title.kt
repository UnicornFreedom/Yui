package totoro.yui.util.api

import org.jsoup.Jsoup
import totoro.yui.Log

object Title {
    private const val useragent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) " +
            "Chrome/59.0.3071.115 Safari/537.36"

    fun get(url: String): String? {
        return try {
            val document = when {
                url.contains("youtu.be") || url.contains("youtube.com") -> Google.downloadDocument(url)
                else -> null
            } ?: Jsoup.connect(url).userAgent(useragent).get()
            val links = document.select("title")
            if (links.isNotEmpty()) links.first()!!.text().replace("\n", " ").replace("\r", "")
            else null
        } catch (e: Exception) {
            Log.error("Jsoup got some problems trying to get a title for: $url")
            Log.debug(e)
            null
        }
    }
}
