package totoro.yui.util.api

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import totoro.yui.util.api.data.Definition
import totoro.yui.util.api.data.Phonetics
import java.net.URL
import java.net.URLEncoder

object Datamuse {
    private const val charset = "UTF-8"

    fun definitions(word: String): List<Definition> {
        val raw = URL("https://api.datamuse.com/words?sp=${URLEncoder.encode(word, charset)}&md=d").readText()
        @Suppress("UNCHECKED_CAST")
        val array = Parser.default().parse(StringBuilder(raw)) as JsonArray<JsonObject>
        return if (array.isNotEmpty()) {
            val json = array.first()
            val subj = json.string("word")
            val defs = json.array<String>("defs")
            return if (defs != null && defs.isNotEmpty()) {
                defs.map { def ->
                    val parts = def.split("\t")
                    Definition(subj.orEmpty(), parts[0], parts[1])
                }
            } else emptyList()
        } else emptyList()
    }

    private fun searchfor(option: String, value: String): List<String> {
        val raw = URL("https://api.datamuse.com/words?$option=${URLEncoder.encode(value, charset)}").readText()
        @Suppress("UNCHECKED_CAST")
        val array = Parser.default().parse(StringBuilder(raw)) as JsonArray<JsonObject>
        return array.mapNotNull { it.string("word") }
    }

    fun thesaurus(word: String): List<String> = searchfor("rel_syn", word)

    fun antonyms(word: String): List<String> = searchfor("rel_ant", word)

    fun word(definition: String): List<String> = searchfor("ml", definition)

    fun rhyme(word: String): List<String> = searchfor("rel_rhy", word)

    fun describe(word: String): List<String> = searchfor("rel_jjb", word)

    fun phonetics(word: String): Phonetics? {
        val raw = URL("https://api.datamuse.com/words?sp=${URLEncoder.encode(word, charset)}&md=r&ipa=1").readText()
        @Suppress("UNCHECKED_CAST")
        val array = Parser.default().parse(StringBuilder(raw)) as JsonArray<JsonObject>
        return if (array.isNotEmpty()) {
            val json = array.first()
            val subj = json.string("word")
            val tags = json.array<String>("tags")
            if (tags != null && tags.size >= 2) {
                val phonetics = tags[1].drop(9)
                return Phonetics(subj.orEmpty(), phonetics)
            } else null
        } else null
    }
}
