package totoro.yui.util.api

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import totoro.yui.util.api.data.Changes

object Changelog {
    private const val rawUrl = "https://gitlab.com/UnicornFreedom/Yui/raw/master/CHANGELOG.md"
    private const val url = "https://gitlab.com/UnicornFreedom/Yui/blob/master/CHANGELOG.md"

    fun getUrl() = url

    fun getChanges(version: String, success: (Changes) -> Unit, failure: () -> Unit) {
        rawUrl.httpGet().responseString { _, _, result ->
            when (result) {
                is Result.Failure -> failure()
                is Result.Success -> {
                    val content = StringBuilder(result.value)
                    val changes = mutableListOf<String>()
                    var recording = false
                    val pattern = "## $version"
                    content.lineSequence().forEach { line ->
                        if (line.startsWith("#")) {
                            recording = line == pattern
                        } else {
                            if (recording) changes.add(line)
                        }
                    }
                    success(Changes(changes, false))
                }
            }
        }
    }
}
