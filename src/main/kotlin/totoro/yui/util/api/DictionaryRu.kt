package totoro.yui.util.api

import org.jsoup.Jsoup
import totoro.yui.Log
import totoro.yui.util.api.data.Definition
import java.net.URLEncoder

object DictionaryRu {
    private const val useragent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) " +
            "Chrome/59.0.3071.115 Safari/537.36"
    private const val minimalLength = 20

    fun definitions(word: String): List<Definition> {
        try {
            return Jsoup.connect("http://www.dict.t-mm.ru/search?text=${URLEncoder.encode(word, "CP1251")}")
                    .userAgent(useragent)
                    .post()
                    .select("div.ArticleText > p")
                    .map { def ->
                        var text = def.text().trim()
                        if (text.toLowerCase().startsWith(word)) text = text.drop(text.indexOf(' ') + 1).trim()
                        text
                    }
                    .filter { it.length >= minimalLength }
                    .map { Definition(word, "", it) }
        } catch (e: Exception) {
            Log.debug(e)
        }
        return emptyList()
    }
}
