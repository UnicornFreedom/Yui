package totoro.yui.util.api

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import totoro.yui.util.api.data.LichessPerf
import totoro.yui.util.api.data.LichessUser

object Lichess {
    fun user(username: String, success: (LichessUser) -> Unit, failure: () -> Unit) {
        "https://lichess.org/api/user/$username".httpGet().responseString { _, _, result ->
            when (result) {
                is Result.Failure -> failure()
                is Result.Success -> {
                    val json = Parser.default().parse(StringBuilder(result.value)) as JsonObject
                    val usernameCorrect = json.string("username")
                    val online = json.boolean("online")
                    val perfs = json.obj("perfs")?.map { entry ->
                        val perfObj = entry.value as JsonObject
                        val rating = perfObj.int("rating")
                        val games = perfObj.int("games")
                        val prov = perfObj.boolean("prov")
                        val prog = perfObj.int("prog")
                        LichessPerf(entry.key, rating!!, games!!, prog!!, prov ?: false)
                    }
                    val playTime = json.obj("playTime")?.int("total")
                    success(LichessUser(usernameCorrect!!, online!!, perfs!!, playTime!!))
                }
            }
        }
    }
}
