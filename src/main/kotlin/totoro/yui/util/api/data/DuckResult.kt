package totoro.yui.util.api.data

@Suppress("unused")
class DuckResult(
        val heading: String?,
        val abstract: String?,
        val abstractUrl: String?,
        val answer: String?,
        val definition: String?,
        val definitionUrl: String?,
        val redirect: String?,
        val firstText: String?,
        val firstUrl: String?
)
