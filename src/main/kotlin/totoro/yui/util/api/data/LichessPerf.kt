package totoro.yui.util.api.data

@Suppress("unused")
class LichessPerf(
    val title: String,
    val rating: Int,
    val games: Int,
    val progress: Int,
    val provisional: Boolean
)
