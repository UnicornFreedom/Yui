package totoro.yui.util.api.data

class Link(val title: String, val url: String)
