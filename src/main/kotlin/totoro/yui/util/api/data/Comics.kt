package totoro.yui.util.api.data

@Suppress("unused")
class Comics (
        val id: Int,
        val title: String,
        val alt: String,
        val date: String
)
