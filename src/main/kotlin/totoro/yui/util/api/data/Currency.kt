package totoro.yui.util.api.data

@Suppress("unused")
class Currency (
        val code: String,
        val price: Double,
        val description: String? = null,
        val delta: Double = 0.0
)
