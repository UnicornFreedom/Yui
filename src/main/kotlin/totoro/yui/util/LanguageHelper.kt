package totoro.yui.util

@Suppress("MemberVisibilityCanBePrivate")
object LanguageHelper {
    private val cyrillic = 'А' .. 'я'
    private val latinLower = ('a' .. 'z')
    private val latinUpper = ('A' .. 'Z')

    fun isLatin(char: Char) = latinLower.contains(char) || latinUpper.contains(char)
    fun isCyrillic(char: Char) = cyrillic.contains(char) || char == 'ё' || char == 'Ё'

    fun detect(message: String): Language =
            detect(message, null)

    fun detect(message: String, exclude: List<Pair<Int, Int>>?): Language {
        var cyr = 0
        var lat = 0
        message.forEachIndexed { index, char ->
            if (exclude == null || exclude.none { it.first <= index && it.first + it.second > index }) {
                if (isLatin(char)) lat++
                if (isCyrillic(char)) cyr++
            }
        }
        return if (cyr > lat)
            Language.RUSSIAN
        else
            Language.ENGLISH
    }
}
