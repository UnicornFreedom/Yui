package totoro.yui

import org.reflections8.Reflections
import totoro.yui.actions.*
import totoro.yui.client.IRCClient
import totoro.yui.db.Database
import totoro.yui.util.Dict
import java.security.GeneralSecurityException
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.*
import javax.net.ssl.*

/**
 * Entry point of the bot
 */

object Yui {
    // do not forget to change version in build.gradle
    private const val Version = "0.23.0"

    private const val ConfigPath = "yui.properties"

    val Random = Random(System.currentTimeMillis())

    fun run() {
        Log.info("Hello. Yui v$Version here.")

        // turn off SSL certificate checking
        // yeah, yeah, I know, this is bad, very bad =)
        val trustAllCertificates = arrayOf<TrustManager>(object : X509TrustManager {
            override fun getAcceptedIssuers(): Array<X509Certificate>? = null
            override fun checkClientTrusted(p0: Array<out X509Certificate>?, p1: String?) {}
            override fun checkServerTrusted(p0: Array<out X509Certificate>?, p1: String?) {}
        })

        val trustAllHostnames = HostnameVerifier { _, _ -> true }

        try {
            val sc = SSLContext.getInstance("SSL")
            sc.init(null, trustAllCertificates, SecureRandom())
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)
            HttpsURLConnection.setDefaultHostnameVerifier(trustAllHostnames)
        } catch (e: GeneralSecurityException) {
            throw ExceptionInInitializerError(e)
        }


        // load configuration if available
        Log.info("Loading setting...")
        Config.load(ConfigPath)

        // connect to database
        Log.info("Connecting to the database...")
        val database = Database("yui.db")
        database.connect()


        // register action processors ...
        Log.info("Registering action processors...")
        val client = IRCClient()
        client.setDatabase(database)

        // ... automatically
        val reflections = Reflections("totoro.yui.actions")
        val actions = reflections.getTypesAnnotatedWith(Action::class.java)
        actions.forEach { action ->
            val instance = action.getDeclaredConstructor().newInstance()
            if (CommandAction::class.java.isAssignableFrom(action)) {
                client.registerCommandAction(instance as CommandAction)
            }
            if (MessageAction::class.java.isAssignableFrom(action)) {
                client.registerMessageAction(instance as MessageAction)
            }
        }

        // ... manually
        client.registerMessageAction(QuoteAction.instance(database))
        client.registerMessageAction(HookAction.instance(database))
        client.registerCommandAction(HookAction.instance(database))
        client.registerCommandAction(WeatherAction(database))
        client.registerCommandAction(LichessAction(database))
        client.registerCommandAction(SimpleAction(Dict.Hello.variants, Dict.Greets))
        client.registerCommandAction(SimpleAction(Dict.Bye.variants + "away", Dict.Bye))
        client.registerCommandAction(SimpleAction(listOf("cookie", "cake", "banana", "apple"),
                Dict.Kawaii + Dict.Excited + Dict.Thanks + Dict.of("oishii", "yummy")))
        client.registerCommandAction(SimpleAction(listOf("v", "version"), Dict.of(Version)))
        client.registerCommandAction(QuoteAction.instance(database))
        client.registerCommandAction(SimpleAction(listOf("anarchy", "rules", "constitution", "revolution"), Dict.of(
                "https://git.io/vwLXq", "sabotage the system!", "no gods, no masters!", "raise hell!",
                "get ready for anarchy!", "welcome to #cc.ru", "there's no government", "don't forget to " +
                "eat your lunch, and make some trouble", "hierarchy is chaos, anarchy is solidarity",
                "keep calm and be an anarchist", "power to the users!", "to have free minds, we must have " +
                "free tea", "chaos & anarchy", "one direction: insurrection, one solution: revolution",
                "i suppose what I believe in is peaceful anarchy", "if I can't dance to it, it's not " +
                "my revolution", "what is important is to spread confusion, not eliminate it", "in a world " +
                "like this one, only the random makes sense", "anarchism is democracy taken seriously",
                "the worst thing in this world, next to anarchy, is government", "anarchy is the only slight " +
                "glimmer of hope", "the internet is the first thing that humanity has built that humanity " +
                "doesn't understand, the largest experiment in anarchy that we have ever had", "there is a " +
                "certain combination of anarchy and discipline in the way I work", "when anarchy is declared, " +
                "the first thing we do, let's kill all the anarchists", "a concept is a brick; it can be used " +
                "to build a courthouse of reason; or it can be thrown through the window", "electricity is " +
                "really just organized lightning", "we started off trying to set up a small anarchist community, " +
                "but people wouldn't obey the rules", "basically, if you're not a utopianist, you're a schmuck",
                "laws ... would be useless on the moon", "if truth is relative, then it’s cousin is anarchy",
                "socialism is an alternative to capitalism as potassium cyanide is an alternative to water",
                "everyone needs rules; after all, how can you break what doesn't exist? rules give anarchy " +
                "something to aim at", "order is just chaos waiting to happen", "every socialist is a disguised " +
                "dictator", "why anarchy? because anything less would be uncivilized", "freedom is the foundation " +
                "for all wonderful things in life", "tomorrow will use you the way you use today", "when desertion " +
                "is not an option, sabotage is a must", "only those prepared to go too far will learn how far " +
                "they can go", "if you take things the wrong way, be aware of which end is up", "one of the great " +
                "things about anarchy is that it’s hard to get it organized", "why, play we, these games of mere men?")))
        client.registerCommandAction(SimpleAction(Dict.Kawaii.variants + "!^nyaa*$", Dict.Kawaii))
        client.registerCommandAction(SimpleAction(Dict.Yeah.variants, Dict.Nope))
        client.registerCommandAction(SimpleAction(Dict.Nope.variants, Dict.Yeah))
        client.registerCommandAction(SimpleAction(Dict.Offended.variants, Dict.Offended))
        client.registerCommandAction(TopAction(database))
        client.registerCommandAction(BlacklistAction(ConfigPath))
        client.registerCommandAction(MarkovAction(database))
        client.registerCommandAction(SimpleAction(listOf("fork", "pitchfork", "!^-+E$"), Dict.of("---E")))
        client.registerCommandAction(SimpleAction(listOf("call", "phone"), Dict.of(
                "hang on a moment, I’ll put you through", "beep-beep-beep...", "rip", "☎",
                "sorry, the balance is not enough", "i’m afraid the line is quite bad",
                "i'm busy at the moment, please leave a message", "the phone is broken")))
        client.registerCommandAction(SimpleAction(listOf("money", "balance", "uu"), Dict.of("https://youtu.be/vm2RAFv4pwA")))
        client.registerCommandAction(SimpleAction(listOf("!^mooo*$", "cow", "cowpowers", "cowsay", "cowsays"),
                Dict.of("to moo or not to moo, that is the question", "meow meow like a cow")))
        client.registerCommandAction(SimpleAction(listOf("swag", "!^.*pony.*$", "crush", "kill", "destroy"),
                Dict.of("CRUSH. KILL. DESTROY. SWAG.")))
        client.registerCommandAction(SimpleAction(listOf("nocode"), Dict.of("https://github.com/kelseyhightower/nocode")))
        client.registerCommandAction(SimpleAction(listOf("disconnect"), Dict.of("disconnect yourself") + Dict.Nope))
        client.registerCommandAction(SimpleAction(listOf("exit", "quit"),
                Dict.of("try /quit", "try Alt+F4", "there's no exit here") + Dict.Nope + Dict.Rip + Dict.Offended))
        client.registerCommandAction(SimpleAction(listOf("nohello"), Dict.of("http://www.nohello.com/")))
        client.registerCommandAction(SimpleAction(listOf("hug"), Dict.of("aww")))
        client.registerCommandAction(SimpleAction(listOf("cat", "kote", "!^meoo*w$", "catpowers", "catsay", "catsays", "neko"),
                Dict.of("~(=^–^)", ":3", "=’①。①’=", "meow", "meooow", "=^._.^=", "/ᐠ｡ꞈ｡ᐟ\\")))
        client.registerCommandAction(SimpleAction(listOf("flip", "table", "drop"),
                Dict.of("(╯°□°）╯︵ ┻━┻", "(ノಠ益ಠ)ノ彡┻━┻", "(╯°□°）╯︵ ┻━┻")))
        client.registerCommandAction(SimpleAction(listOf("shrug"), Dict.of("¯\\_(ツ)_/¯")))
        client.registerCommandAction(SimpleAction(listOf("turbo", "turbofish"), Dict.of("::<>", "https://turbo.fish/", "<>::")))
        client.registerCommandAction(SimpleAction(listOf("calmdown", "cooldown"),
                Dict.of("https://meduza.io/feature/2017/07/03/vse-besit-kak-perestat-besitsya-po-lyubomu-povodu-instruktsiya")))
        client.registerCommandAction(SimpleAction(listOf("powered", "poweredby", "credits"),
                Dict.of("i'm created with the power of Kotlin, Kitteh IRC lib, Debian and also the forest spirit power :3",
                        "i'm a product of some creative meddling with letters and digits")))
        client.registerCommandAction(SimpleAction(listOf("upgrade"), Dict.of("https://www.youtube.com/watch?v=rzLSmY7c9dY")))
        client.registerCommandAction(SimpleAction(listOf("troll", "arch", "trolling"), Dict.of("take this: `pacman -Syu`")))
        client.registerCommandAction(SimpleAction(listOf("compile", "make", "cmake", "gcc", "build", "cargo", "gradle"), Dict.of(
                "irc.cpp:8:28: missing terminating \" character", "moo.cpp: ld returned 1 exit status",
                "error[E2066]: Invalid MOM inheritance", "error[E2502]: Error resolving #import: Rust is too rusted",
                "error[E2497]: No GUID associated with type: 'fish'", "error[E2427]: 'fork' cannot be a template function",
                "error[E2252]: 'catch' expected", "error[E2323]: Illegal number suffix", "error[E2370]: Simple type name expected",
                "rip.cpp:12:1: null pointer assignment", "error[E2014]: Member is ambiguous: 'gentoo' and 'rippo'",
                "irc.rs:4:20: missing a bunch of opening brackets", "error[E0463]: can't find crate for `uu_contrib`")))
        client.registerCommandAction(SimpleAction(listOf("vk", "vkontakte", "group", "public", "wall"),
                Dict.of("https://vk.com/hashccru")))
        client.registerCommandAction(SimpleAction(listOf("logs", "log", "history"),
                Dict.of("https://logs.fomalhaut.me/")))
        client.registerCommandAction(SimpleAction(listOf("quotes"),
                Dict.of("https://quotes.fomalhaut.me/")))
        client.registerCommandAction(SimpleAction(listOf("oc", "ocelot", "emulator", "emul"),
                Dict.of("https://ocelot.fomalhaut.me/")))
        client.registerCommandAction(SimpleAction(listOf("mastodon"),
                Dict.of("https://mastodon.social/@hashccru")))
        client.registerCommandAction(SimpleAction(listOf("hel", "repo"),
                Dict.of("https://hel.fomalhaut.me/")))
        client.registerCommandAction(SimpleAction(listOf("hpm"),
                Dict.of("pastebin run vf6upeAN")))
        client.registerCommandAction(SimpleAction(listOf("party"),
                Dict.of("https://soundcloud.com/masonm27/hollywood-undead-party-by-myself-5")))
        client.registerCommandAction(SimpleAction(listOf("joined", "another"),
                Dict.of("https://www.youtube.com/watch?v=PNups4pAPr8")))
        client.registerCommandAction(SimpleAction(listOf("github"),
                Dict.of("https://github.com/cc-ru")))
        client.registerCommandAction(SimpleAction(listOf("gitlab"),
                Dict.of("https://gitlab.com/cc-ru")))
        client.registerCommandAction(SimpleAction(listOf("blog"),
                Dict.of("https://cc-ru.gitlab.io/")))
        client.registerCommandAction(SimpleAction(listOf("discord"),
                Dict.of("https://discord.gg/FM9qWGm")))
        client.registerCommandAction(SimpleAction(listOf("mousu", "mausu", "mouse", "maus", "^^"),
                Dict.of("https://www.youtube.com/watch?v=zkkXL_g4JHs")))
        client.registerCommandAction(SimpleAction(listOf("wednesday", "wednesdayos", "windows"),
                Dict.of("https://www.youtube.com/watch?v=Oct2xKMGOno")))
        client.registerCommandAction(SimpleAction(listOf("source", "sources", "src"), Dict.of(
                "hey! it's indecent - to ask such things from a girl",
                "hey! it's indecent - to ask such things from a bot",
                "I'd prefer not to answer that", "that's too private",
                "do you even know Kotlin?", "show me your own sources first",
                "let’s talk about something else", "ask totoro",
                "'nothing' is the source of everything, and 'everything' will become nothing one day",
                "try to search the web", "i'm a quite complicated being, try something easier first")))
        client.registerCommandAction(SimpleAction(listOf("git"), Dict.of("https://git-scm.com/")))
        client.registerCommandAction(SimpleAction(listOf("birth", "birthday", "bd"), Dict.of("6/6/17 at 12:07 AM UTC+02")))
        client.registerCommandAction(SimpleAction(listOf("!^nap..itan.*$"), Dict.of(
                "https://www.youtube.com/watch?v=6lY730l84aI",
                "https://www.youtube.com/watch?v=nJz57eIwRZo",
                "https://www.youtube.com/watch?v=6TFeGkjN6Rc")))
        client.registerCommandAction(SimpleAction(listOf("!^crab.*", "!.*\uD83E\uDD80.*", "kani", "カニ"),
            Dict.of("\uD83E\uDD80", "\uD83E\uDD80\uD83E\uDD80", "\uD83E\uDD80\uD83E\uDD80\uD83E\uDD80",
                "\uD83E\uDD80\uD83E\uDD80\uD83E\uDD80\uD83E\uDD80", "\uD83E\uDD80\uD83E\uDD80\uD83E\uDD80\uD83E\uDD80\uD83E\uDD80")))
        client.registerCommandAction(SimpleAction(listOf("communism", "communist", "lenin"),
                Dict.of("https://www.youtube.com/watch?v=l3TvORNCMfw")))

        Log.info("... aaaaand I'm initialized.")

        // let's go!
        client.start {
            client.login(Config.pass)
            client.broadcast(Dict.Greets())
        }
    }
}

fun main() {
    Yui.run()
}
