package totoro.yui.client

/**
 * Small utility class containing all useful info about a received command
 *
 * Examples of input split:
 *
 * ~coin 100 USD to CZK
 *  ^---                name
 *       ^-- ^-- ^- ^-- args
 *       ^------------- content
 *
 * ~base64:decode apple
 *  ^-----              name
 *         ^-----       modifier
 *                ^---- args
 *                ^---- content
 */

class Command(val chan: String, val user: String, val original: String) {
    companion object {
        fun parse(message: String, channel: String, user: String, botName: String): Command? {
            val trimmed = message.trim()
            return when {
                trimmed.startsWith("~") ->
                    Command(channel, user, trimmed.drop(1))
                trimmed.startsWith(botName) ->
                    Command(channel, user, trimmed.drop(trimmed.indexOfFirst { it.isWhitespace() } + 1))
                else ->
                    null
            }
        }
    }

    @Suppress("MemberVisibilityCanPrivate")
    private val words = original.split(' ', '\t', '\r', '\n').filterNot { it.isEmpty() }
    private val firstWord = words.getOrNull(0)?.toLowerCase()
    val name = (
        if (firstWord?.contains(":") == true) firstWord.substring(0, firstWord.indexOf(":"))
        else firstWord
    )?.toLowerCase()
    val modifier =
        if (firstWord?.contains(":") == true) firstWord.substring(firstWord.indexOf(":") + 1)
        else null
    val content = original.drop(firstWord?.length ?: 0).trim()
    val args = words.drop(1)
    val valid = name != null
}
