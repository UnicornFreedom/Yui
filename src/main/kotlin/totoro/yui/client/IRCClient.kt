package totoro.yui.client

import net.engio.mbassy.listener.Handler
import org.kitteh.irc.client.library.Client
import org.kitteh.irc.client.library.element.User
import org.kitteh.irc.client.library.event.channel.ChannelMessageEvent
import org.kitteh.irc.client.library.event.channel.UnexpectedChannelLeaveViaKickEvent
import org.kitteh.irc.client.library.event.channel.UnexpectedChannelLeaveViaPartEvent
import org.kitteh.irc.client.library.event.client.ClientNegotiationCompleteEvent
import org.kitteh.irc.client.library.event.client.ClientReceiveMotdEvent
import org.kitteh.irc.client.library.event.connection.ClientConnectionClosedEvent
import org.kitteh.irc.client.library.event.connection.ClientConnectionEstablishedEvent
import org.kitteh.irc.client.library.event.user.PrivateMessageEvent
import totoro.yui.Config
import totoro.yui.Log
import totoro.yui.actions.CommandAction
import totoro.yui.actions.MessageAction
import totoro.yui.db.Database
import totoro.yui.util.Dict

@Suppress("unused", "UNUSED_PARAMETER", "MemberVisibilityCanBePrivate")
class IRCClient {
    private val coolNicknames = Dict.of("yui`", "yuki`", "yumi`", "ayumi`")

    private val nick = coolNicknames()
    private val realname = "Yui the Bot"

    val history = History(100)
    private val client = Client.builder()
            .nick(nick)
            .user(nick)
            .name(nick)
            .realName(realname)
            .server()
            .host(Config.host)
            .then().build()

    private val commandActions = ArrayList<CommandAction>()
    private val messageActions = ArrayList<MessageAction>()

    private var onceConnected: (() -> Unit)? = null

    private var database: Database? = null

    init {
        Config.chan.forEach { client.addChannel(it) }
        client.eventManager.registerEventListener(this)
    }


    fun connect() {
        client.connect()
    }

    fun login(pass: String?) {
        if (pass != null) {
            client.sendMessage("nickserv", "identify $pass")
            Log.info("Logged in.")
        }
    }

    fun broadcast(message: String) {
        Config.chan.forEach { client.sendMessage(it, message) }
        Log.outgoing(message)
    }

    fun send(chan: String, message: String, limit: Int = Config.floodLimit) {
        if (message.contains('\n') || message.contains('\r')) {
            send(chan, message.split('\n', '\r'), limit)
        } else {
            client.sendMessage(chan, message)
            Log.outgoing("[$chan] $message")
        }
    }

    fun send(chan: String, messages: List<String>, limit: Int = Config.floodLimit) {
        val nonEmpty = messages.filter { it.isNotEmpty() }
        (if (nonEmpty.size > limit) nonEmpty.take(limit - 1).plus("...") else nonEmpty).map {
            send(chan, it)
        }
    }

    fun start(onceConnected: () -> Unit) {
        connect()
        this.onceConnected = onceConnected
    }


    fun registerCommandAction(action: CommandAction): Boolean {
        // mark the processor with accordance to the settings
        action.blocked = Config.blackCommands.any { action.correspondsName(it) }
        action.adminsOnly = Config.adminCommands.any { action.correspondsName(it) }
        // and add it to the list of processors, with position determined by the command popularity
        for ((index, value) in commandActions.withIndex()) {
            if (value.priority.ordinal >= action.priority.ordinal) {
                commandActions.add(index, action)
                return true
            }
        }
        commandActions.add(action)
        return true
    }

    fun registerMessageAction(action: MessageAction) {
        messageActions.add(action)
    }

    fun setDatabase(database: Database) {
        this.database = database
    }


    fun getListOfUsersOnline(): List<User> =
            client.channels.flatMap { it.users }

    fun getListOfNicknamesOnline(channel: String): List<String> {
        val option = client.getChannel(channel)
        return if (option.isPresent) option.get().nicknames
        else listOf()
    }

    fun getListOfNicknamesOnline(): List<String> =
            client.channels.flatMap { it.nicknames }

    fun isUserOnline(nickname: String): Boolean =
            getListOfNicknamesOnline().any { it == nickname }

    fun isBroteOnline(): Boolean =
            isUserOnline("brote")

    fun getUserHost(nickname: String): String? =
            getListOfUsersOnline().firstOrNull { it.nick == nickname }?.host

    fun getMessagePrefixLength(): Int {
        val user = client.user.get()
        return user.userString.length + 1 + user.host.length
    }

    fun getFirstActionProcessor(chan: String, user: String, command: String): CommandAction {
        return getFirstActionProcessor(Command(chan, user, command))
    }

    fun getFirstActionProcessor(command: Command): CommandAction {
        return commandActions.first { it.willProcess(this, command) }
    }

    fun getActionProcessorsIter() = commandActions.iterator()

    private fun processMessage(chan: String, user: String, message: String) {
        Log.incoming("[${ if (chan == user) "PM" else chan }] $user: $message")
        if (!tryActionProcessors(chan, user, message))
            history.add(chan, user, message)
    }
    private fun tryActionProcessors(chan: String, user: String, message: String): Boolean {
        val command = Command.parse(message, chan, user, nick)
        if (command != null) {
            // if the command was parsed successfully we will try to process it via command processors
            if (Config.blackUsers.contains(user))
                send(chan, "$user: totoro says you are baka " + Dict.Offended())
            else if (!Config.blackUsersSilent.contains(user)) {
                // call registered action processors
                @Suppress("LoopToCallChain")
                for (action in commandActions) {
                    // we use here the `willProcess` method, that gives preliminary decision on whether or not
                    // the command must be processed
                    // the `processCommand` method can later decline the command - it will be passed to the next processor
                    if (action.willProcess(this, command)) {
                        // check permission marks
                        if (action.blocked && user != "Totoro") {
                            send(chan, "totoro says - don't use the ~${command.name} command " + Dict.KawaiiEmoji())
                            return false
                        }
                        if (action.adminsOnly && user !in Config.admins && user != "Totoro") {
                            send(chan, "totoro says - only admins can use the ~${command.name} command " + Dict.KawaiiEmoji())
                            return false
                        }
                        // command can be consumed by one of processors
                        // in this case we do not need to try the rest of actions
                        if (action.processCommand(this, command) == null) {
                            database?.stats?.increment(action.name())
                            return true
                        }
                    }
                }
            }
        } else {
            // if the message cannot be interpreted as a correct command then we will try common message processors
            for (action in messageActions) {
                if (action.processMessage(this, chan, user, message) == null) return false
            }
        }
        return false
    }


    @Handler
    fun motd(event: ClientReceiveMotdEvent) {
        if (onceConnected != null) {
            onceConnected?.invoke()
            onceConnected = null
        }
    }

    @Handler
    fun meow(event: ClientNegotiationCompleteEvent) =
        Log.info("I'm ready to chat (today I'm $nick).")

    @Handler
    fun ready(event: ClientConnectionEstablishedEvent) =
        Log.info("I'm connected!")

    @Handler
    fun kawaii(event: ClientConnectionClosedEvent) =
        Log.info("I'm disconnected! ${Dict.Upset()}")

    @Handler
    fun baka(event: UnexpectedChannelLeaveViaKickEvent) {
        Log.info("I was kicked! ${Dict.Offended}")
        event.channel.join()
    }

    @Handler
    fun baka(event: UnexpectedChannelLeaveViaPartEvent) {
        Log.info("I did leave, but I do not remember why...")
        event.channel.join()
    }

    @Handler
    fun incoming(event: ChannelMessageEvent) {
        processMessage(event.channel.name, event.actor.nick, event.message)
    }

    @Handler
    fun private(event: PrivateMessageEvent) {
        if (Config.pm) {
            processMessage(event.actor.nick, event.actor.nick, event.message)
        }
    }
}
