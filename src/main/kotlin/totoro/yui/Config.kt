package totoro.yui

import org.jetbrains.kotlin.konan.file.File
import org.jetbrains.kotlin.konan.properties.saveToFile
import totoro.yui.util.LogLevel
import java.io.FileInputStream
import java.util.*

@Suppress("SameParameterValue")
object Config {
    private val splitRegex = Regex("\\s*,\\s*")
    private val prop = Properties()

    private const val ADMINS_KEY = "admins"
    private const val HOST_KEY = "host"
    private const val CHAN_KEY = "chan"
    private const val PASS_KEY = "pass"
    private const val BLACK_COMMANDS_KEY = "black_commands"
    private const val BLACK_USERS_KEY = "black_users"
    private const val BLACK_USERS_SILENT_KEY = "black_users_silent"
    private const val PM_KEY = "pm"
    private const val LOG_LEVEL_KEY = "log_level"
    private const val MARKOV_PATH_KEY = "markov_path"
    private const val MARKOV_ORDER_KEY = "markov_order"
    private const val MARKOV_ALLOW_NON_LETTERS_KEY = "markov_allow_non_letters"
    private const val MARKOV_ALLOW_SHORT_LINES_KEY = "markov_allow_short_lines"
    private const val ADMIN_COMMANDS_KEY = "admin_commands"
    private const val FLOOD_LIMIT_KEY = "flood_limit"

    const val maxMessageLength = 498

    var admins: List<String> = listOf()
    lateinit var host: String
    lateinit var chan: List<String>
    var pass: String? = null
    var blackCommands: List<String> = listOf()
    var blackUsers: MutableList<String> = mutableListOf()
    var blackUsersSilent: MutableList<String> = mutableListOf()
    var pm: Boolean = true
    var logLevel: LogLevel = LogLevel.DEBUG
    var markovPath: String? = null
    var markovOrder: Int = 1
    var markovAllowNonLetters = true
    var markovAllowShortLines = false
    var adminCommands: List<String> = listOf()
    var floodLimit: Int = 5

    fun load(filepath: String) {
        try {
            FileInputStream(filepath).use {
                prop.load(it)
                admins = getList(ADMINS_KEY, listOf())
                host = getString(HOST_KEY, "irc.esper.net")
                chan = getList(CHAN_KEY, listOf("#meowbeast"))
                pass = getString(PASS_KEY, "")
                blackCommands = getList(BLACK_COMMANDS_KEY, listOf())
                blackUsers = getList(BLACK_USERS_KEY, listOf()).toMutableList()
                blackUsersSilent = getList(BLACK_USERS_SILENT_KEY, listOf()).toMutableList()
                pm = getBoolean(PM_KEY, true)
                val rawLogLevel = getString(LOG_LEVEL_KEY, "debug").toLowerCase()
                logLevel = LogLevel.deserialize(rawLogLevel)
                markovPath = getString(MARKOV_PATH_KEY, "")
                markovOrder = getInt(MARKOV_ORDER_KEY, markovOrder)
                markovAllowNonLetters = getBoolean(MARKOV_ALLOW_NON_LETTERS_KEY, true)
                markovAllowShortLines = getBoolean(MARKOV_ALLOW_SHORT_LINES_KEY, false)
                adminCommands = getList(ADMIN_COMMANDS_KEY, listOf())
                floodLimit = getInt(FLOOD_LIMIT_KEY, 5)
            }
        } catch (e: Exception) {
            Log.warn("Cannot load the configuration file, setting default values.")
            host = "irc.esper.net"
            chan = listOf("#meowbeast")
        }
    }

    fun save(filepath: String) {
        try {
            setList(ADMINS_KEY, admins)
            setString(HOST_KEY, host)
            setList(CHAN_KEY, chan)
            setString(PASS_KEY, pass ?: "")
            setList(BLACK_COMMANDS_KEY, blackCommands)
            setList(BLACK_USERS_KEY, blackUsers)
            setList(BLACK_USERS_SILENT_KEY, blackUsersSilent)
            setBoolean(PM_KEY, pm)
            setString(LOG_LEVEL_KEY, LogLevel.serialize(logLevel))
            setString(MARKOV_PATH_KEY, markovPath ?: "")
            setInt(MARKOV_ORDER_KEY, markovOrder)
            setBoolean(MARKOV_ALLOW_NON_LETTERS_KEY, markovAllowNonLetters)
            setBoolean(MARKOV_ALLOW_SHORT_LINES_KEY, markovAllowShortLines)
            setList(ADMIN_COMMANDS_KEY, adminCommands)
            setInt(FLOOD_LIMIT_KEY, floodLimit)

            prop.saveToFile(File(filepath))
        } catch (e: Exception) {
            Log.error("Cannot save the configuration file.")
            Log.debug(e)
        }
    }

    private fun getString(key: String, default: String): String {
        return prop.getProperty(key) ?: default
    }
    private fun getList(key: String, default: List<String>): List<String> {
        return prop.getProperty(key)?.split(splitRegex) ?: default
    }
    private fun getBoolean(key: String, default: Boolean): Boolean {
        return if (prop.containsKey(key))
            prop.getProperty(key) == "true"
        else
            default
    }
    @Suppress("SameParameterValue")
    private fun getInt(key: String, default: Int): Int {
        return try {
            prop.getProperty(key)?.toInt() ?: default
        } catch (e: Exception) {
            default
        }
    }

    private fun setString(key: String, value: String) {
        prop.setProperty(key, value)
    }
    private fun setList(key: String, list: List<String>) {
        prop.setProperty(key, list.joinToString(", "))
    }
    private fun setBoolean(key: String, value: Boolean) {
        prop.setProperty(key, if (value) "true" else "false")
    }
    private fun setInt(key: String, value: Int) {
        prop.setProperty(key, value.toString())
    }
}
