package totoro.yui.db

import totoro.yui.actions.data.Popularity
import java.sql.Connection
import java.sql.ResultSet

class StatsTable(connection: Connection) : Table(connection, "stats") {

    fun init() {
        update("create table if not exists $table (name text primary key, popularity integer) without rowid;")
    }

    fun set(name: String, popularity: Long) {
        insert("insert into $table (name, popularity) values('$name', $popularity);")
    }

    fun increment(name: String) {
        val popularity = get(name)
        if (popularity != null)
            update("update $table set popularity = popularity + 1 where name = '$name';")
        else
            set(name, 1)
    }

    fun get(name: String): Long? {
        return firstFrom(query("select * from $table where name = '$name';"))
    }

    fun getTop(n: Int): List<Popularity> {
        return allFrom(query("select * from $table order by popularity desc limit $n;"))
    }

    private fun allFrom(result: ResultSet): List<Popularity> {
        val records: MutableList<Popularity> = mutableListOf()
        while (result.next()) {
            records.add(Popularity(
                result.getString("name"),
                result.getLong("popularity")
            ))
        }
        result.close()
        return records
    }

    private fun firstFrom(result: ResultSet): Long? {
        var value: Long? = null
        if (result.next()) {
            value = result.getLong("popularity")
        }
        result.close()
        return value
    }
}
