package totoro.yui.db

import java.sql.Connection
import java.sql.ResultSet

open class PersonalRecordTable(connection: Connection, table: String) : Table(connection, table) {

    fun init() {
        update("create table if not exists $table (nickname text primary key, location text) without rowid;")
    }

    fun set(nickname: String, location: String) {
        val record = get(nickname)
        val statement = if (record == null)
            connection.prepareStatement("insert into $table (location, nickname) values (?, ?);")
        else
            connection.prepareStatement("update $table set location = ? where nickname = ?;")
        statement.setString(1, location)
        statement.setString(2, nickname)
        statement.executeUpdate()
    }

    fun get(nickname: String): String? {
        return firstFrom(query("select * from $table where nickname = '$nickname';"))
    }

    private fun firstFrom(result: ResultSet): String? {
        var location: String? = null
        if (result.next()) {
            location = result.getString("location")
        }
        result.close()
        return location
    }
}
