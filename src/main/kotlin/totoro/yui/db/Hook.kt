package totoro.yui.db

data class Hook(val id: Long, val hunter: String, val target: String, val text: String)
