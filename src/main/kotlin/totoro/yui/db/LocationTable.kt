package totoro.yui.db

import java.sql.Connection

class LocationTable(connection: Connection) : PersonalRecordTable(connection, "location")
